### This repository contains the different notebooks that were used for the Functionality Benchmark test.

### In the jars.zip folder the different jars needed can be found. They were compiled either using "sbt" or "maven". Their version is in the file name, but it could be they are already outdated, though for the tests purposes are more than enough.

### GeoPySpark benchmark test was done in Jupyter Notebooks.

### Magellan and GeoSpark tests were done in Databricks notebooks.


